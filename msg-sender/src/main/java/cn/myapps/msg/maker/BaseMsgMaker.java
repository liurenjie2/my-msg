package cn.myapps.msg.maker;

import cn.myapps.msg.vo.MsgCommon;
import org.apache.velocity.VelocityContext;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 消息加工器基类
 * @Author: nicholas
 * @Date:   2020-06-10
 * @Version: V1.0
 */
public abstract class BaseMsgMaker {
    /**
     * 获取模板引擎上下文
     *
     * @param msgData 消息数据
     * @return VelocityContext 模板引擎上下文
     */
    VelocityContext getVelocityContext(Map<String, Object> msgData) {
        VelocityContext velocityContext = new VelocityContext();
        for (String key : msgData.keySet()) {
            velocityContext.put(key, msgData.get(key));
        }
        return velocityContext;
    }

    Map<String, Object> toMsgData(MsgCommon msg){
        Map<String, Object> rtn = new HashMap<>();
        rtn.put("from", msg.getFrom());
        rtn.put("to", msg.getTo());
        rtn.put("bizNo", msg.getBizNo());
        rtn.put("title", msg.getTitle());
        rtn.put("content", msg.getContent());
        rtn.put("templateCode", msg.getTemplateCode());

        if (msg.getOtherParams() != null) {
            rtn.putAll(msg.getOtherParams());
        }

        return rtn;
    }
}
