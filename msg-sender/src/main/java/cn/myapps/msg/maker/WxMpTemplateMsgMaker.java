package cn.myapps.msg.maker;

import cn.myapps.msg.util.TemplateUtil;
import cn.myapps.msg.vo.MsgCommon;
import cn.myapps.msg.vo.MsgTemplate;
import cn.myapps.msg.vo.WxTemplateData;
import cn.myapps.msg.wx.entity.MsgMpTemplate;
import cn.myapps.msg.wx.vo.WxMpTemplate;
import com.alibaba.fastjson.JSON;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;

import java.util.List;
import java.util.Map;

/**
 * @Description: 公众号模板消息加工器
 * @Author: nicholas
 * @Date:   2020-06-10
 * @Version: V1.0
 */
public class WxMpTemplateMsgMaker extends BaseMsgMaker implements IMsgMaker {

    public String templateId;

    public String templateCode;

    private String templateUrl;

    private String maAppid;

    private String maPagePath;

    /**
     * 准备(界面字段等)
     */
    @Override
    public void prepare(MsgTemplate template) {
        templateId = template.getId();
        templateCode = template.getTemplateCode();

        if (template instanceof WxMpTemplate) {
            WxMpTemplate wxTemplate =  (WxMpTemplate)template;
            templateUrl = wxTemplate.getTemplateUrl();
            maAppid = wxTemplate.getMaAppid();
            maPagePath = wxTemplate.getMaPagePath();
        }
    }

    /**
     * 组织模板消息-公众号
     *
     * @param msg 消息数据
     * @return WxMpTemplateMessage
     */
    @Override
    public MsgMpTemplate makeMsg(MsgCommon msg) {

        MsgMpTemplate msgMp = MsgMpTemplate.build(msg);

        Map<String,Object> msgData = toMsgData(msg);

        VelocityContext velocityContext = getVelocityContext(msgData);

        if (StringUtils.isNotBlank(templateUrl) && msgData.containsKey("url")) {
            String templateUrlEvaluated = TemplateUtil.evaluate(templateUrl, velocityContext);
            msgMp.setUrl(templateUrlEvaluated);
        }
        // 小程序appId
        if (msgData.containsKey("maAppid")) { // 从数据获取
            msgMp.setMaAppid((String)msgData.get("maAppid"));
        } else { // 从模板获取
            msgMp.setMaAppid(maAppid);
        }

        // 小程序链接
        if (StringUtils.isNotBlank(maPagePath) && msgData.containsKey("maPagePath")) {
            String miniAppPagePathEvaluated = TemplateUtil.evaluate(maPagePath, velocityContext);
            msgMp.setMaPagePath(miniAppPagePathEvaluated);
        }

        return msgMp;
    }

    public WxMpTemplateMessage toWxMpTemplateMessage(MsgMpTemplate msgMp) {
        // 拼模板
        WxMpTemplateMessage wxMessageTemplate = new WxMpTemplateMessage();
        wxMessageTemplate.setTemplateId(msgMp.getTemplateId());
        wxMessageTemplate.setUrl(msgMp.getUrl());

        if (StringUtils.isNotBlank(msgMp.getMaAppid())) {
            WxMpTemplateMessage.MiniProgram miniProgram = new WxMpTemplateMessage.MiniProgram(msgMp.getMaAppid(),
                    msgMp.getMaPagePath(), false);
            wxMessageTemplate.setMiniProgram(miniProgram);
        }

        if (StringUtils.isNotBlank(msgMp.getTemplateDataJson())) {
            List<WxTemplateData> list = JSON.parseArray(msgMp.getTemplateDataJson(), WxTemplateData.class);

            for (WxTemplateData templateData : list) {
                WxMpTemplateData wxMpTemplateData = new WxMpTemplateData();
                wxMpTemplateData.setName(templateData.getName());
                wxMpTemplateData.setValue(templateData.getValue());
                wxMpTemplateData.setColor(templateData.getColor());
                wxMessageTemplate.addData(wxMpTemplateData);
            }

            wxMessageTemplate.setToUser(msgMp.getMsgTo());
        }

        return wxMessageTemplate;
    }
}
