package cn.myapps.msg.maker;

import cn.myapps.msg.vo.MsgCommon;
import cn.myapps.msg.vo.MsgTemplate;

/**
 * @Description: 消息构建
 * @Author: nicholas
 * @Date:   2020-06-10
 * @Version: V1.0
 */
public interface IMsgMaker {
    /**
     * 准备(模板元素)
     */
    void prepare(MsgTemplate template);

    /**
     * 消息加工器接口
     *
     * @return Object
     */
    Object makeMsg(MsgCommon msg);
}
