package cn.myapps.msg.maker;

import cn.myapps.msg.enums.MsgTypeEnum;

/**
 * @Description: 消息加工器工厂类
 * @Author: nicholas
 * @Date:   2020-06-10
 * @Version: V1.0
 */
public class MsgMakerFactory {

    /**
     * 获取消息加工器
     *
     * @return IMsgMaker
     */
    public static IMsgMaker getMsgMaker(int msgType) {
        IMsgMaker iMsgMaker = null;
        switch (msgType) {
            case MsgTypeEnum.MP_TEMPLATE_CODE:
                //iMsgMaker = new WxMpTemplateMsgMaker();
                break;
            case MsgTypeEnum.MA_TEMPLATE_CODE:
                //iMsgMaker = new WxMaTemplateMsgMaker();
                break;
            case MsgTypeEnum.MA_SUBSCRIBE_CODE:
                //iMsgMaker = new WxMaSubscribeMsgMaker();
                break;
            case MsgTypeEnum.KEFU_CODE:
                //iMsgMaker = new WxKefuMsgMaker();
                break;
            case MsgTypeEnum.ALI_YUN_CODE:
                //iMsgMaker = new AliyunMsgMaker();
                break;
            case MsgTypeEnum.TX_YUN_CODE:
                //iMsgMaker = new TxYunMsgMaker();
                break;
            case MsgTypeEnum.HW_YUN_CODE:
                //iMsgMaker = new HwYunMsgMaker();
                break;
            case MsgTypeEnum.YUN_PIAN_CODE:
                //iMsgMaker = new YunPianMsgMaker();
                break;
            case MsgTypeEnum.EMAIL_CODE:
                //iMsgMaker = new MailMsgMaker();
                break;
            case MsgTypeEnum.WX_CP_CODE:
                iMsgMaker = new WxCpMsgMaker();
                break;
            case MsgTypeEnum.HTTP_CODE:
                //iMsgMaker = new HttpMsgMaker();
                break;
            case MsgTypeEnum.DING_CODE:
                //iMsgMaker = new DingMsgMaker();
                break;
            case MsgTypeEnum.BD_YUN_CODE:
                //iMsgMaker = new BdYunMsgMaker();
                break;
            case MsgTypeEnum.UP_YUN_CODE:
                //iMsgMaker = new UpYunMsgMaker();
                break;
            case MsgTypeEnum.QI_NIU_YUN_CODE:
                //iMsgMaker = new QiNiuYunMsgMaker();
                break;
            default:
        }

        return iMsgMaker;
    }
}
