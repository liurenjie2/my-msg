package cn.myapps.msg.maker;

import cn.myapps.msg.enums.MsgTypeEnum;
import cn.myapps.msg.util.TemplateUtil;
import cn.myapps.msg.vo.MsgCommon;
import cn.myapps.msg.vo.MsgTemplate;
import cn.myapps.msg.wx.entity.MsgWxCp;
import cn.myapps.msg.wx.vo.WxCpTemplate;
import cn.myapps.msg.wx.vo.WxMpTemplate;
import me.chanjar.weixin.cp.bean.WxCpMessage;
import me.chanjar.weixin.cp.bean.article.NewArticle;
import org.apache.velocity.VelocityContext;

/**
 * @Description: 企业号消息加工器
 * @Author: nicholas
 * @Date:   2020-06-19
 * @Version: V1.0
 */
public class WxCpMsgMaker extends BaseMsgMaker implements IMsgMaker {

    private String msgTitle;
    private String msgContent;

    private String picUrl;
    public String desc;
    public String url;
    private String btnTxt;
    public String msgType;
    private String templateCode;
    private String templateId;

    /**
     * 准备(界面字段等)
     */
    @Override
    public void prepare(MsgTemplate template) {
        msgTitle = template.getTitle();
        msgContent = template.getContent();
        templateCode = template.getTemplateCode();
        templateId = template.getId();


        if (template instanceof WxCpTemplate) {
            WxCpTemplate wxCptemplate =  (WxCpTemplate)template;
            picUrl = wxCptemplate.getPicUrl();
            desc = wxCptemplate.getDesc();
            url = wxCptemplate.getUrl();
            btnTxt = wxCptemplate.getBtnTxt();
            msgType = wxCptemplate.getMsgType();
            templateCode = wxCptemplate.getTemplateCode();
        }
    }

    /**
     * 组织消息-企业号
     *
     * @param msg 消息数据
     * @return WxMpTemplateMessage
     */
    @Override
    public MsgWxCp makeMsg(MsgCommon msg) {
        MsgWxCp msgWxCp = new MsgWxCp();
        VelocityContext velocityContext = getVelocityContext(toMsgData(msg));

        String agentId = getAgentId(msg);
        msgWxCp.setAgentId(agentId);
        msgWxCp.setMsgTo(msg.getTo());
        msgWxCp.setCpMsgType(msgType);
        msgWxCp.setMsgType(MsgTypeEnum.WX_CP.getCode());
        msgWxCp.setCompanyUid(msg.getCompanyUid());
        msgWxCp.setTemplateId(templateId);

        if ("图文消息".equals(msgType)) {
            // 标题
            String title = TemplateUtil.evaluate(msgTitle, velocityContext);
            msgWxCp.setTitle(title);
            // 图片url
            String picUrlLink = TemplateUtil.evaluate(picUrl, velocityContext);
            msgWxCp.setPicUrl(picUrlLink);
            // 描述
            String description = TemplateUtil.evaluate(desc, velocityContext);
            msgWxCp.setDescription(description);
            // 跳转url
            String urlLink = TemplateUtil.evaluate(url, velocityContext);
            msgWxCp.setUrl(urlLink);
        } else if ("文本消息".equals(msgType)) {
            String content = TemplateUtil.evaluate(msgContent, velocityContext);
            msgWxCp.setContent(content);
        } else if ("markdown消息".equals(msgType)) {
            String content = TemplateUtil.evaluate(msgContent, velocityContext);
            msgWxCp.setContent(content);
        } else if ("文本卡片消息".equals(msgType)) {
            // 标题
            String title = TemplateUtil.evaluate(msgTitle, velocityContext);
            // 描述
            String description = TemplateUtil.evaluate(desc, velocityContext);
            // 跳转url
            String urlLink = TemplateUtil.evaluate(url, velocityContext);
            // 按钮文本
            String btnTxt = TemplateUtil.evaluate(this.btnTxt, velocityContext);

            msgWxCp.setTitle(title);
            msgWxCp.setDescription(description);
            msgWxCp.setUrl(urlLink);
            msgWxCp.setBtnTxt(btnTxt);
        }

        return msgWxCp;
    }

    private String getAgentId(MsgCommon msg){
        if (msg.getOtherParams().containsKey("agentId")){
            return (String) msg.getOtherParams().get("agentId");
        }

        return null;
    }

    public WxCpMessage toWxCpMessage(MsgWxCp msgWxCp){
        WxCpMessage wxCpMessage = null;

        if ("图文消息".equals(msgType)) {
            NewArticle article = new NewArticle();
            // 标题
            article.setTitle(msgWxCp.getTitle());
            // 图片url
            article.setPicUrl(msgWxCp.getPicUrl());
            // 描述
            article.setDescription(msgWxCp.getDescription());
            // 跳转url
            article.setUrl(msgWxCp.getUrl());

            wxCpMessage = WxCpMessage.NEWS().addArticle(article).build();
        } else if ("文本消息".equals(msgType)) {
            wxCpMessage = WxCpMessage.TEXT().agentId(Integer.valueOf(msgWxCp.getAgentId()))
                    .toUser(msgWxCp.getMsgTo()).content(msgWxCp.getContent()).build();
        } else if ("markdown消息".equals(msgType)) {
            wxCpMessage = WxCpMessage.MARKDOWN().agentId(Integer.valueOf(msgWxCp.getAgentId()))
                    .toUser(msgWxCp.getMsgTo()).content(msgWxCp.getContent()).build();
        } else if ("文本卡片消息".equals(msgType)) {
            wxCpMessage = WxCpMessage.TEXTCARD().agentId(Integer.valueOf(msgWxCp.getAgentId()))
                    .toUser(msgWxCp.getMsgTo()).title(msgWxCp.getTitle())
                    .description(msgWxCp.getDescription()).url(msgWxCp.getUrl()).btnTxt(msgWxCp.getBtnTxt()).build();
        }

        wxCpMessage.setToUser(msgWxCp.getMsgTo());

        return wxCpMessage;
    }
}
