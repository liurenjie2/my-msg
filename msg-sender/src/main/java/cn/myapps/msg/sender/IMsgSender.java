package cn.myapps.msg.sender;

import cn.myapps.msg.vo.MsgCommon;
import cn.myapps.msg.vo.SendResult;

import java.util.Map;

/**
 * @Description: 消息发送器接口
 * @Author: nicholas
 * @Date:   2020-06-10
 * @Version: V1.0
 */
public interface IMsgSender {

    /**
     * 发送消息
     */
    SendResult send(MsgCommon msg);

    /**
     * 发送模板消息
     *
     * @param msg
     * @return
     */
    SendResult sendByTemplate(MsgCommon msg);

    /**
     * 异步发送消息
     *
     * @param msg 消息数据
     */
    SendResult asyncSend(MsgCommon msg);
}
