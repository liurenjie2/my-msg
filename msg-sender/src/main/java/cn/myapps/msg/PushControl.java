package cn.myapps.msg;

/**
 * @Description: 推送控制
 * @Author: nicholas
 * @Date:   2020-06-10
 * @Version: V1.0
 */
public class PushControl {

    /**
     * 是否空跑
     */
    public static boolean dryRun;

    public volatile static boolean saveResponseBody = false;

    /**
     * 模板变量前缀
     */
    public static final String TEMPLATE_VAR_PREFIX = "var";
}