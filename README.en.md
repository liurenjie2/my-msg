# my-msg

#### Description
消息推送服务，与各大主流平台进行对接

目前已经支持的消息类型：
- 模板消息-公众号
- 模板消息-小程序
- 订阅消息-小程序
- 微信客服消息
- 微信企业号/企业微信消息
- 小程序统一服务消息
- 钉钉
- 阿里云短信
- 阿里大于模板短信
- E-Mail
- HTTP请求（单次、批量、压测）

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
