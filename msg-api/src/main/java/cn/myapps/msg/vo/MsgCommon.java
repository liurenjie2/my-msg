package cn.myapps.msg.vo;

import com.alibaba.fastjson.JSON;
import lombok.Builder;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 通用消息，简化发送
 */
@Data
public class MsgCommon {
    /**
     * 消息类型
     */
    private int msgType;
    /**
     * 模板编码
     */
    private String templateCode;
    /**
     * 发送者
     */
    private String from;
    /**
     * 接收者, 支持多个以;号分隔
     */
    private String to;
    /**
     * 主题
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    /**
     * 业务编号
     */
    private String bizNo;
    /**
     * 企业UID
     */
    private String companyUid;
    /**
     * 其他参数
     */
    private Map<String, Object> otherParams = new HashMap<>();

    /**
     * 是否以模板形式发送
     *
     * @return
     */
    public boolean isSendByTemplate() {
        if (templateCode != null && templateCode.trim().length() > 0) {
            return true;
        }

        return false;
    }
}
