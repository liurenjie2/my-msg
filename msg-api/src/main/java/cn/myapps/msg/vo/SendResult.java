package cn.myapps.msg.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 发送结果
 *
 * @author nicholas
 * @since 2020.
 */
@Getter
@Setter
@ToString
public class SendResult {
    private boolean success = false;

    private String info;

    public static SendResult success(){
        SendResult result = new SendResult();
        result.setSuccess(true);

        return result;
    }

    public static SendResult fail(String info){
        SendResult result = new SendResult();
        result.setSuccess(false);
        result.setInfo(info);

        return result;
    }
}
