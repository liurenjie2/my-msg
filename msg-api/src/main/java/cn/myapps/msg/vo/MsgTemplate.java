package cn.myapps.msg.vo;

public interface MsgTemplate {
    public String getId();
    public String getTemplateCode();
    public String getTitle();
    public String getContent();
}
