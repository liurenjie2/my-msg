package cn.myapps.msg.vo;

import lombok.Data;

@Data
public class MailFile {
    private String fileName;

    private String fileURI;

    private byte[] fileContent;
}
