package cn.myapps.msg.mail.service;

import cn.myapps.msg.mail.entity.MailAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 邮件服务器配置
 * @Author: jeecg-boot
 * @Date:   2020-06-09
 * @Version: V1.0
 */
public interface IMailAccountService extends IService<MailAccount> {
    public MailAccount findByKey(String key);

    public MailAccount findByMsgFrom(String msgFrom);
}
