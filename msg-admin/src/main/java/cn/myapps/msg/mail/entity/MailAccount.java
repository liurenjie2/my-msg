package cn.myapps.msg.mail.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 邮件服务器配置
 * @Author: nicholas
 * @Date:   2020-06-10
 * @Version: V1.0
 */
@Data
@TableName("my_mail_account")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="my_mail_account对象", description="邮件服务器配置")
public class MailAccount {
    
	/**id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
	private java.lang.Integer id;
	/**创建日期*/
	@Excel(name = "创建日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新日期*/
	@Excel(name = "更新日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
	private java.lang.String createrName;
	/**更新人*/
	@Excel(name = "更新人", width = 15)
    @ApiModelProperty(value = "更新人")
	private java.lang.String updaterName;
	/**邮件服务器地址*/
	@Excel(name = "邮件服务器地址", width = 15)
    @ApiModelProperty(value = "邮件服务器地址")
	private java.lang.String host;
	/**邮件服务器端口*/
	@Excel(name = "邮件服务器端口", width = 15)
    @ApiModelProperty(value = "邮件服务器端口")
	private java.lang.Integer port;
	/**是否需要身份验证*/
	@Excel(name = "是否需要身份验证", width = 15)
    @ApiModelProperty(value = "是否需要身份验证")
	private Boolean auth;
	/**邮件默认发送者*/
	@Excel(name = "邮件默认发送者", width = 15)
    @ApiModelProperty(value = "邮件默认发送者")
	private java.lang.String msgFrom;
	/**邮箱账号*/
	@Excel(name = "邮箱账号", width = 15)
    @ApiModelProperty(value = "邮箱账号")
	private java.lang.String user;
	/**邮箱账号密码*/
	@Excel(name = "邮箱账号密码", width = 15)
    @ApiModelProperty(value = "邮箱账号密码")
	private java.lang.String pass;
	/**是否使用ssl安全连接*/
	@Excel(name = "是否使用ssl安全连接", width = 15)
    @ApiModelProperty(value = "是否使用ssl安全连接")
	private Boolean sslEnable;
	/**是否使用startttls安全连接*/
	@Excel(name = "是否使用startttls安全连接", width = 15)
    @ApiModelProperty(value = "是否使用startttls安全连接")
	private Boolean startttlsEnable;
	/**配置唯一标识*/
	@Excel(name = "配置唯一标识", width = 15)
    @ApiModelProperty(value = "配置唯一标识")
	private java.lang.String accountKey;
	@Excel(name = "企业UID", width = 15)
	@ApiModelProperty(value = "企业UID")
	private java.lang.String companyUid;
}
