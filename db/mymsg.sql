/*
Navicat MySQL Data Transfer

Source Server         : ops-qa
Source Server Version : 50716
Source Host           : 192.168.200.31:3306
Source Database       : mymsg

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2020-07-02 09:22:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for my_ding_app
-- ----------------------------
DROP TABLE IF EXISTS `my_ding_app`;
CREATE TABLE `my_ding_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) DEFAULT NULL,
  `agent_id` varchar(255) DEFAULT NULL,
  `app_key` varchar(255) DEFAULT NULL,
  `app_secret` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_mail_account
-- ----------------------------
DROP TABLE IF EXISTS `my_mail_account`;
CREATE TABLE `my_mail_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_key` varchar(100) DEFAULT NULL COMMENT '配置唯一标识',
  `host` varchar(100) DEFAULT NULL COMMENT '邮件服务器地址',
  `port` int(11) DEFAULT NULL COMMENT '邮件服务器端口',
  `auth` bit(1) NOT NULL COMMENT '是否需要身份验证',
  `msg_from` varchar(100) DEFAULT NULL COMMENT '邮件默认发送者',
  `user` varchar(100) DEFAULT NULL COMMENT '邮箱账号',
  `pass` varchar(100) DEFAULT NULL COMMENT '邮箱账号密码',
  `ssl_enable` bit(1) NOT NULL COMMENT '是否使用ssl安全连接',
  `startttls_enable` bit(1) NOT NULL COMMENT '是否使用startttls安全连接',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `creater_name` varchar(100) DEFAULT NULL COMMENT '创建人',
  `updater_name` varchar(100) DEFAULT NULL COMMENT '更新人',
  `company_uid` varchar(100) DEFAULT NULL COMMENT '企业UID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_key` (`account_key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_mail_template
-- ----------------------------
DROP TABLE IF EXISTS `my_mail_template`;
CREATE TABLE `my_mail_template` (
  `id` varchar(50) NOT NULL COMMENT '模板UUID',
  `template_name` varchar(100) DEFAULT NULL COMMENT '模板名称',
  `template_code` varchar(50) DEFAULT NULL COMMENT '模板编码',
  `mail_title` varchar(255) DEFAULT NULL COMMENT '邮件模板主题',
  `mail_cc` varchar(255) DEFAULT NULL COMMENT '邮件模板抄送',
  `mail_content` text COMMENT '邮件模板内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `creater_name` varchar(100) DEFAULT NULL COMMENT '创建人',
  `updater_name` varchar(100) DEFAULT NULL COMMENT '更新人',
  `company_uid` varchar(100) DEFAULT NULL COMMENT '企业UID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_ding
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_ding`;
CREATE TABLE `my_msg_ding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `radio_type` varchar(255) DEFAULT NULL,
  `ding_msg_type` varchar(255) DEFAULT NULL,
  `agent_id` varchar(255) DEFAULT NULL,
  `web_hook` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_http
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_http`;
CREATE TABLE `my_msg_http` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `params` varchar(255) DEFAULT NULL,
  `headers` varchar(255) DEFAULT NULL,
  `cookies` varchar(255) DEFAULT NULL,
  `body` varchar(255) DEFAULT NULL,
  `body_type` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_kefu
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_kefu`;
CREATE TABLE `my_msg_kefu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `kefu_msg_type` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `describes` text,
  `url` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_kefu_priority
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_kefu_priority`;
CREATE TABLE `my_msg_kefu_priority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `template_id` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `ma_appid` varchar(255) DEFAULT NULL,
  `ma_page_path` varchar(255) DEFAULT NULL,
  `kefu_msg_type` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `describes` text,
  `kefu_url` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_mail
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_mail`;
CREATE TABLE `my_msg_mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL COMMENT '消息类型',
  `msg_status` int(11) DEFAULT NULL COMMENT '消息状态: 0-未发送;1-已发送',
  `title` varchar(100) DEFAULT NULL COMMENT '邮件主题',
  `cc` varchar(2000) DEFAULT NULL COMMENT '抄送',
  `file_json` mediumtext COMMENT '邮件文件JSON',
  `content` text COMMENT '邮件内容',
  `msg_from` varchar(100) DEFAULT NULL COMMENT '发送者',
  `msg_to` varchar(100) DEFAULT NULL COMMENT '接收者,可以多个以;号分隔',
  `biz_no` varchar(100) DEFAULT NULL COMMENT '业务编码',
  `field1` varchar(255) DEFAULT NULL COMMENT '扩展字段1',
  `field2` varchar(255) DEFAULT NULL COMMENT '扩展字段2',
  `template_id` varchar(50) DEFAULT NULL COMMENT '邮件模板id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `company_uid` varchar(100) DEFAULT NULL COMMENT '企业UID',
  `error` text COMMENT '异常信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_ma_subscribe
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_ma_subscribe`;
CREATE TABLE `my_msg_ma_subscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `template_id` varchar(255) DEFAULT NULL,
  `page` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_ma_template
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_ma_template`;
CREATE TABLE `my_msg_ma_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `template_id` varchar(255) DEFAULT NULL,
  `page` varchar(255) DEFAULT NULL,
  `emphasis_keyword` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_mp_template
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_mp_template`;
CREATE TABLE `my_msg_mp_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL COMMENT '消息类型',
  `msg_status` int(11) DEFAULT NULL COMMENT '消息状态',
  `template_id` varchar(50) DEFAULT NULL COMMENT '公众号消息模板ID',
  `template_data_json` text COMMENT '模板数据JSON',
  `msg_id` varchar(50) DEFAULT NULL COMMENT '公众号消息ID',
  `url` varchar(255) DEFAULT NULL COMMENT '消息链接',
  `msg_from` varchar(100) DEFAULT NULL COMMENT '发送者',
  `msg_to` varchar(100) DEFAULT NULL COMMENT '接收者',
  `ma_appid` varchar(100) DEFAULT NULL COMMENT '关联小程序appId',
  `ma_page_path` varchar(255) DEFAULT NULL COMMENT '关联小程序页面路径',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `company_uid` varchar(100) DEFAULT NULL COMMENT '企业UID',
  `error` text COMMENT '异常信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_send_history
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_send_history`;
CREATE TABLE `my_msg_send_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) DEFAULT NULL,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `csv_file` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_sms
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_sms`;
CREATE TABLE `my_msg_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `template_id` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_wx_cp
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_wx_cp`;
CREATE TABLE `my_msg_wx_cp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL COMMENT '消息类型',
  `msg_status` int(11) DEFAULT NULL COMMENT '消息状态',
  `template_id` varchar(50) DEFAULT NULL COMMENT '消息模板ID',
  `cp_msg_type` varchar(50) DEFAULT NULL COMMENT '企业微信消息类型',
  `agent_id` varchar(50) DEFAULT NULL COMMENT '企业应用agentId',
  `msg_from` varchar(100) DEFAULT NULL COMMENT '发送者',
  `msg_to` varchar(100) DEFAULT NULL COMMENT '接收者',
  `msg_to_type` int(11) DEFAULT NULL COMMENT '接收者类型',
  `content` text COMMENT '消息内容',
  `title` varchar(100) DEFAULT NULL COMMENT '消息标题',
  `pic_url` varchar(255) DEFAULT NULL COMMENT '消息图片链接',
  `description` text COMMENT '描述',
  `url` varchar(255) DEFAULT NULL COMMENT '消息链接',
  `btn_txt` varchar(255) DEFAULT NULL COMMENT '按钮文本',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `company_uid` varchar(100) DEFAULT NULL COMMENT '企业UID',
  `error` text COMMENT '异常信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_msg_wx_uniform
-- ----------------------------
DROP TABLE IF EXISTS `my_msg_wx_uniform`;
CREATE TABLE `my_msg_wx_uniform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_name` varchar(255) DEFAULT NULL,
  `mp_template_id` varchar(255) DEFAULT NULL,
  `ma_template_id` varchar(255) DEFAULT NULL,
  `mp_url` varchar(255) DEFAULT NULL,
  `ma_appid` varchar(255) DEFAULT NULL,
  `ma_page_path` varchar(255) DEFAULT NULL,
  `page` varchar(255) DEFAULT NULL,
  `emphasis_keyword` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_template_data
-- ----------------------------
DROP TABLE IF EXISTS `my_template_data`;
CREATE TABLE `my_template_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) DEFAULT NULL,
  `msg_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_wx_account
-- ----------------------------
DROP TABLE IF EXISTS `my_wx_account`;
CREATE TABLE `my_wx_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_type` varchar(50) DEFAULT NULL COMMENT '账号类型',
  `account_name` varchar(100) DEFAULT NULL COMMENT '账号名称',
  `app_id` varchar(100) DEFAULT NULL COMMENT '微信账号appid',
  `app_secret` varchar(100) DEFAULT NULL COMMENT '微信账号appSecret',
  `token` varchar(100) DEFAULT NULL COMMENT '微信账号token',
  `aes_key` varchar(255) DEFAULT NULL COMMENT '微信账号aesKey',
  `max_thread_pool` int(11) NOT NULL COMMENT '异步发送时的线程池大小',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `creater_name` varchar(100) DEFAULT NULL COMMENT '创建人',
  `updater_name` varchar(100) DEFAULT NULL COMMENT '更新人',
  `company_uid` varchar(100) DEFAULT NULL COMMENT '企业UID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_wx_cp_app
-- ----------------------------
DROP TABLE IF EXISTS `my_wx_cp_app`;
CREATE TABLE `my_wx_cp_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corp_id` varchar(100) DEFAULT NULL COMMENT '企业corpId',
  `app_name` varchar(100) DEFAULT NULL COMMENT '企业应用名称',
  `agent_id` varchar(100) DEFAULT NULL COMMENT '企业应用agentId',
  `secret` varchar(100) DEFAULT NULL COMMENT '企业应用secret',
  `token` varchar(100) DEFAULT NULL COMMENT '企业应用token',
  `aes_key` varchar(255) DEFAULT NULL COMMENT '企业应用aesKey',
  `max_conn_pool` int(11) NOT NULL DEFAULT '5' COMMENT '连接池大小',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `creater_name` varchar(100) DEFAULT NULL COMMENT '创建人名称',
  `updater_name` varchar(100) DEFAULT NULL COMMENT '更新人名称',
  `company_uid` varchar(100) DEFAULT NULL COMMENT '企业UID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for my_wx_mp_user
-- ----------------------------
DROP TABLE IF EXISTS `my_wx_mp_user`;
CREATE TABLE `my_wx_mp_user` (
  `open_id` varchar(255) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `sex_desc` varchar(255) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `head_img_url` varchar(255) DEFAULT NULL,
  `subscribe_time` varchar(255) DEFAULT NULL,
  `union_id` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `subscribe_scene` varchar(255) DEFAULT NULL,
  `qr_scene` varchar(255) DEFAULT NULL,
  `qr_scene_str` varchar(255) DEFAULT NULL,
  `create_time` varchar(255) DEFAULT NULL,
  `modified_time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`open_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='InnoDB free: 504832 kB; (`SCHED_NAME` `TRIGGER_NAME` `TRIGGE';

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(200) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='InnoDB free: 504832 kB; (`SCHED_NAME` `TRIGGER_NAME` `TRIGGE';

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='InnoDB free: 504832 kB; (`SCHED_NAME` `TRIGGER_NAME` `TRIGGE';

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='InnoDB free: 504832 kB; (`SCHED_NAME` `TRIGGER_NAME` `TRIGGE';

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='InnoDB free: 504832 kB; (`SCHED_NAME` `JOB_NAME` `JOB_GROUP`';
