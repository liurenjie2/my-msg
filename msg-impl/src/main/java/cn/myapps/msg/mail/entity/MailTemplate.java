package cn.myapps.msg.mail.entity;

import java.io.Serializable;
import java.util.Date;

import cn.myapps.msg.vo.MsgTemplate;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 邮件发送模板
 * @Author: nicholas
 * @Date:   2020-06-10
 * @Version: V1.0
 */
@Data
@TableName("my_mail_template")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="my_mail_template对象", description="邮件发送模板")
public class MailTemplate implements MsgTemplate {
    
	/**模板UUID*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "模板UUID")
	private java.lang.String id;
	/**模板名称*/
	@Excel(name = "模板名称", width = 15)
    @ApiModelProperty(value = "模板名称")
	private java.lang.String templateName;
	/**模板编码*/
	@Excel(name = "模板编码", width = 15)
    @ApiModelProperty(value = "模板编码")
	private java.lang.String templateCode;
	/**邮件模板主题*/
	@Excel(name = "邮件模板主题", width = 15)
    @ApiModelProperty(value = "邮件模板主题")
	@TableField("mail_title")
	private java.lang.String title;
	/**邮件模板抄送*/
	@Excel(name = "邮件模板抄送", width = 15)
    @ApiModelProperty(value = "邮件模板抄送")
	private java.lang.String mailCc;
	/**邮件模板内容*/
	@Excel(name = "邮件模板内容", width = 15)
    @ApiModelProperty(value = "邮件模板内容")
	@TableField("mail_content")
	private java.lang.String content;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private java.util.Date createTime;
	/**更新时间*/
	@Excel(name = "更新时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private java.util.Date updateTime;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
	private java.lang.String createrName;
	/**更新人*/
	@Excel(name = "更新人", width = 15)
    @ApiModelProperty(value = "更新人")
	private java.lang.String updaterName;
	@Excel(name = "企业UID", width = 15)
	@ApiModelProperty(value = "企业UID")
	private java.lang.String companyUid;
}
