package cn.myapps.msg.mail.service.impl;

import cn.myapps.msg.mail.entity.MsgMail;
import cn.myapps.msg.mail.mapper.MsgMailMapper;
import cn.myapps.msg.mail.service.IMsgMailService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 邮件信息
 * @Author: jeecg-boot
 * @Date:   2020-06-09
 * @Version: V1.0
 */
@Service
public class MsgMailServiceImpl extends ServiceImpl<MsgMailMapper, MsgMail> implements IMsgMailService {

}
