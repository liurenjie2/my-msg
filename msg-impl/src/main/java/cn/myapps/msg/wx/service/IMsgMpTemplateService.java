package cn.myapps.msg.wx.service;

import cn.myapps.msg.wx.entity.MsgMpTemplate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 微信模板消息
 * @Author: jeecg-boot
 * @Date:   2020-06-12
 * @Version: V1.0
 */
public interface IMsgMpTemplateService extends IService<MsgMpTemplate> {

}
