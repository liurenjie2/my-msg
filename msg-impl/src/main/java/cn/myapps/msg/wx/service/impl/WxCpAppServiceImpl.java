package cn.myapps.msg.wx.service.impl;

import cn.myapps.msg.wx.entity.WxCpApp;
import cn.myapps.msg.wx.mapper.WxCpAppMapper;
import cn.myapps.msg.wx.service.IWxCpAppService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 微信企业应用配置
 * @Author: jeecg-boot
 * @Date:   2020-06-18
 * @Version: V1.0
 */
@Service
public class WxCpAppServiceImpl extends ServiceImpl<WxCpAppMapper, WxCpApp> implements IWxCpAppService {

    @Override
    public WxCpApp findByAgentId(String agentId) {
        LambdaQueryWrapper<WxCpApp> lambdaQuery = Wrappers.<WxCpApp>lambdaQuery();
        lambdaQuery.eq(WxCpApp::getAgentId, agentId);

        return getOne(lambdaQuery);
    }

    @Override
    public WxCpApp findByAppName(String appName) {
        LambdaQueryWrapper<WxCpApp> lambdaQuery = Wrappers.<WxCpApp>lambdaQuery();
        lambdaQuery.eq(WxCpApp::getAppName, appName);

        return getOne(lambdaQuery);
    }
}
