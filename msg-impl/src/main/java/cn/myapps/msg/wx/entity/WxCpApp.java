package cn.myapps.msg.wx.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 微信企业应用配置
 * @Author: jeecg-boot
 * @Date:   2020-06-18
 * @Version: V1.0
 */
@Data
@TableName("my_wx_cp_app")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="my_wx_cp_app对象", description="微信企业应用配置")
public class WxCpApp {
    
	/**id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
	private java.lang.Integer id;
	/**企业corpId*/
	@Excel(name = "企业corpId", width = 15)
    @ApiModelProperty(value = "企业corpId")
	private java.lang.String corpId;
	/**企业应用名称*/
	@Excel(name = "企业应用名称", width = 15)
    @ApiModelProperty(value = "企业应用名称")
	private java.lang.String appName;
	/**企业应用agentId*/
	@Excel(name = "企业应用agentId", width = 15)
    @ApiModelProperty(value = "企业应用agentId")
	private java.lang.String agentId;
	/**企业应用secret*/
	@Excel(name = "企业应用secret", width = 15)
    @ApiModelProperty(value = "企业应用secret")
	private java.lang.String secret;
	/**企业应用token*/
	@Excel(name = "企业应用token", width = 15)
    @ApiModelProperty(value = "企业应用token")
	private java.lang.String token;
	/**企业应用aesKey*/
	@Excel(name = "企业应用aesKey", width = 15)
    @ApiModelProperty(value = "企业应用aesKey")
	private java.lang.String aesKey;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private java.util.Date createTime;
	/**更新时间*/
	@Excel(name = "更新时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private java.util.Date updateTime;
	/**创建人名称*/
	@Excel(name = "创建人名称", width = 15)
    @ApiModelProperty(value = "创建人名称")
	private java.lang.String createrName;
	/**更新人名称*/
	@Excel(name = "更新人名称", width = 15)
    @ApiModelProperty(value = "更新人名称")
	private java.lang.String updaterName;
	/**企业UID*/
	@Excel(name = "企业UID", width = 15)
    @ApiModelProperty(value = "企业UID")
	private java.lang.String companyUid;
	/**连接池大小, 默认为5*/
	@Excel(name = "连接池大小", width = 15)
	@ApiModelProperty(value = "连接池大小")
	private java.lang.Integer maxConnPool;
}
