package cn.myapps.msg.wx.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 企业微信消息
 * @Author: jeecg-boot
 * @Date:   2020-06-18
 * @Version: V1.0
 */
@Data
@TableName("my_msg_wx_cp")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="my_msg_wx_cp对象", description="企业微信消息")
public class MsgWxCp {
    
	/**id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
	private java.lang.Integer id;
	/**消息类型*/
	@Excel(name = "消息类型", width = 15)
    @ApiModelProperty(value = "消息类型")
	private java.lang.Integer msgType;
	/**消息状态*/
	@Excel(name = "消息状态", width = 15)
    @ApiModelProperty(value = "消息状态")
	private java.lang.Integer msgStatus;
	/**消息模板ID*/
	@Excel(name = "消息模板ID", width = 15)
    @ApiModelProperty(value = "消息模板ID")
	private java.lang.String templateId;
	/**企业微信消息类型*/
	@Excel(name = "企业微信消息类型", width = 15)
    @ApiModelProperty(value = "企业微信消息类型")
	private java.lang.String cpMsgType;
	/**企业应用agentId*/
	@Excel(name = "企业应用agentId", width = 15)
    @ApiModelProperty(value = "企业应用agentId")
	private java.lang.String agentId;
	/**发送者*/
	@Excel(name = "发送者", width = 15)
    @ApiModelProperty(value = "发送者")
	private java.lang.String msgFrom;
	/**接收者*/
	@Excel(name = "接收者", width = 15)
    @ApiModelProperty(value = "接收者")
	private java.lang.String msgTo;
	/**接收者类型*/
	@Excel(name = "接收者类型", width = 15)
    @ApiModelProperty(value = "接收者类型")
	private java.lang.Integer msgToType;
	/**消息内容*/
	@Excel(name = "消息内容", width = 15)
    @ApiModelProperty(value = "消息内容")
	private java.lang.String content;
	/**title*/
	@Excel(name = "消息标题", width = 15)
    @ApiModelProperty(value = "消息标题")
	private java.lang.String title;
	/**消息图片链接*/
	@Excel(name = "消息图片链接", width = 15)
    @ApiModelProperty(value = "消息图片链接")
	private java.lang.String picUrl;
	/**描述*/
	@Excel(name = "描述", width = 15)
    @ApiModelProperty(value = "描述")
	private java.lang.String description;
	/**消息链接*/
	@Excel(name = "消息链接", width = 15)
    @ApiModelProperty(value = "消息链接")
	private java.lang.String url;
	/**按钮文本*/
	@Excel(name = "按钮文本", width = 15)
	@ApiModelProperty(value = "按钮文本")
	private java.lang.String btnTxt;
	/**createTime*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private java.util.Date createTime;
	/**更新时间*/
	@Excel(name = "更新时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private java.util.Date updateTime;
	/**企业UID*/
	@Excel(name = "企业UID", width = 15)
    @ApiModelProperty(value = "企业UID")
	private java.lang.String companyUid;
	/**异常信息*/
	@Excel(name = "异常信息", width = 15)
    @ApiModelProperty(value = "异常信息")
	private java.lang.String error;
}
