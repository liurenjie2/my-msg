package cn.myapps.msg.wx.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 微信公众号/小程序
 * @Author: jeecg-boot
 * @Date:   2020-06-12
 * @Version: V1.0
 */
@Data
@TableName("my_wx_account")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="my_wx_account对象", description="微信公众号/小程序")
public class WxAccount {
    
	/**id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
	private Integer id;
	/**账号类型*/
	@Excel(name = "账号类型", width = 15)
    @ApiModelProperty(value = "账号类型")
	private String accountType;
	/**账号名称*/
	@Excel(name = "账号名称", width = 15)
    @ApiModelProperty(value = "账号名称")
	private String accountName;
	/**微信账号appid*/
	@Excel(name = "微信账号appid", width = 15)
    @ApiModelProperty(value = "微信账号appid")
	private String appId;
	/**微信账号appSecret*/
	@Excel(name = "微信账号appSecret", width = 15)
    @ApiModelProperty(value = "微信账号appSecret")
	private String appSecret;
	/**微信账号token*/
	@Excel(name = "微信账号token", width = 15)
    @ApiModelProperty(value = "微信账号token")
	private String token;
	/**微信账号aesKey*/
	@Excel(name = "微信账号aesKey", width = 15)
    @ApiModelProperty(value = "微信账号aesKey")
	private String aesKey;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**更新时间*/
	@Excel(name = "更新时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private Date updateTime;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
	private String createrName;
	/**更新人*/
	@Excel(name = "更新人", width = 15)
    @ApiModelProperty(value = "更新人")
	private String updaterName;
	/**企业UID*/
	@Excel(name = "企业UID", width = 15)
    @ApiModelProperty(value = "企业UID")
	private String companyUid;
	/**企业UID*/
	@Excel(name = "异步发送时的线程池大小", width = 15)
	@ApiModelProperty(value = "异步发送时的线程池大小")
	private int maxThreadPool = 5;
}
