package cn.myapps.msg.wx.vo;

import cn.myapps.msg.vo.MsgTemplate;

public class WxMpTemplate implements MsgTemplate {
    protected String id;
    protected String templateCode;
    protected String templateUrl;
    protected String maAppid;
    protected String maPagePath;

    public WxMpTemplate(String templateCode){
        this.templateCode = templateCode;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    @Override
    public String getTemplateCode() {
        return templateCode;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getContent() {
        return null;
    }

    public String getTemplateUrl() {
        return templateUrl;
    }

    public void setTemplateUrl(String templateUrl) {
        this.templateUrl = templateUrl;
    }

    public String getMaAppid() {
        return maAppid;
    }

    public void setMaAppid(String maAppid) {
        this.maAppid = maAppid;
    }

    public String getMaPagePath() {
        return maPagePath;
    }

    public void setMaPagePath(String maPagePath) {
        this.maPagePath = maPagePath;
    }
}
