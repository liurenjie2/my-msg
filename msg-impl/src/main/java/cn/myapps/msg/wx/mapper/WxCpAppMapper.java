package cn.myapps.msg.wx.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.myapps.msg.wx.entity.WxCpApp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 微信企业应用配置
 * @Author: jeecg-boot
 * @Date:   2020-06-18
 * @Version: V1.0
 */
public interface WxCpAppMapper extends BaseMapper<WxCpApp> {

}
