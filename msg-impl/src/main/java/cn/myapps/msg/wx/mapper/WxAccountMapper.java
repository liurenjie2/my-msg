package cn.myapps.msg.wx.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.myapps.msg.wx.entity.WxAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 微信公众号/小程序
 * @Author: jeecg-boot
 * @Date:   2020-06-12
 * @Version: V1.0
 */
public interface WxAccountMapper extends BaseMapper<WxAccount> {

}
