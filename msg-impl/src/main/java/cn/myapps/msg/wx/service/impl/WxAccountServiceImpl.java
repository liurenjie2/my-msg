package cn.myapps.msg.wx.service.impl;

import cn.myapps.msg.wx.entity.WxAccount;
import cn.myapps.msg.wx.mapper.WxAccountMapper;
import cn.myapps.msg.wx.service.IWxAccountService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 微信公众号/小程序
 * @Author: jeecg-boot
 * @Date:   2020-06-12
 * @Version: V1.0
 */
@Service
public class WxAccountServiceImpl extends ServiceImpl<WxAccountMapper, WxAccount> implements IWxAccountService {

    @Override
    public WxAccount findByAppId(String appId) {
        LambdaQueryWrapper<WxAccount> lambdaQuery = Wrappers.<WxAccount>lambdaQuery();
        lambdaQuery.eq(WxAccount::getAppId, appId);

        return getOne(lambdaQuery);
    }

    @Override
    public WxAccount findByAccountName(String accountName) {
        LambdaQueryWrapper<WxAccount> lambdaQuery = Wrappers.<WxAccount>lambdaQuery();
        lambdaQuery.eq(WxAccount::getAccountName, accountName);

        return getOne(lambdaQuery);
    }
}
