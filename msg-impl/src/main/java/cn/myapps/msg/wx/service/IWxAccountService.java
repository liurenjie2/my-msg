package cn.myapps.msg.wx.service;

import cn.myapps.msg.wx.entity.WxAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 微信公众号/小程序
 * @Author: nicholas
 * @Date:   2020-06-12
 * @Version: V1.0
 */
public interface IWxAccountService extends IService<WxAccount> {
    public WxAccount findByAppId(String appId);

    WxAccount findByAccountName(String accountName);
}
