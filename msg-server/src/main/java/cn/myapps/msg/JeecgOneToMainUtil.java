package cn.myapps.msg;

import org.jeecgframework.codegenerate.generate.impl.CodeGenerateOneToMany;
import org.jeecgframework.codegenerate.generate.pojo.onetomany.MainTableVo;
import org.jeecgframework.codegenerate.generate.pojo.onetomany.SubTableVo;

import java.util.ArrayList;
import java.util.List;

/**
 * 代码生成器入口【一对多】
 * @Author 张代浩
 * @site www.jeecg.org
 * 
 */
public class JeecgOneToMainUtil {

	/**
	 * 一对多(父子表)数据模型，生成方法
	 * @param args
	 */
	public static void main(String[] args) {
		//第一步：设置主表配置
		MainTableVo mainTable = new MainTableVo();
		mainTable.setTableName("oms_order");//表名
		mainTable.setEntityName("Order");	 //实体名
		mainTable.setEntityPackage("oms");	 //包名
		mainTable.setFtlDescription("总订单");	 //描述
		
		//第二步：设置子表集合配置
		List<SubTableVo> subTables = new ArrayList<SubTableVo>();
		
		//[0].子表一
		SubTableVo po = new SubTableVo();
		po.setTableName("oms_so_base");//表名
		po.setEntityName("ServiceOrderBase");	    //实体名
		po.setEntityPackage("oms");	        //包名
		po.setFtlDescription("服务项");       //描述
		//子表外键参数配置
		/*说明: 
		 * a) 子表引用主表主键ID作为外键，外键字段必须以_ID结尾;
		 * b) 主表和子表的外键字段名字，必须相同（除主键ID外）;
		 * c) 多个外键字段，采用逗号分隔;
		*/
		po.setForeignKeys(new String[]{"order_id"});
		subTables.add(po);
		
		//[1].子表二
		SubTableVo po1 = new SubTableVo();
		po1.setTableName("oms_order_cargo");//表名
		po1.setEntityName("CargoInfo");	    //实体名
		po1.setEntityPackage("oms");	        //包名
		po1.setFtlDescription("商品信息");       //描述
		//子表外键参数配置
		/*说明: 
		 * a) 子表引用主表主键ID作为外键，外键字段必须以_ID结尾;
		 * b) 主表和子表的外键字段名字，必须相同（除主键ID外）;
		 * c) 多个外键字段，采用逗号分隔;
		*/
		po1.setForeignKeys(new String[]{"order_id"});
		subTables.add(po1);
		
		//[2].子表三
		SubTableVo po2 = new SubTableVo();
		po2.setTableName("oms_order_cargo_amount");		//表名
		po2.setEntityName("CargoAmount");			//实体名
		po2.setEntityPackage("oms"); 				//包名
		po2.setFtlDescription("货量信息");			//描述
		//子表外键参数配置
		/*说明: 
		 * a) 子表引用主表主键ID作为外键，外键字段必须以_ID结尾;
		 * b) 主表和子表的外键字段名字，必须相同（除主键ID外）;
		 * c) 多个外键字段，采用逗号分隔;
		*/
		po2.setForeignKeys(new String[]{"order_id"});
		subTables.add(po2);
		mainTable.setSubTables(subTables);
		
		//[3].子表四
		SubTableVo po3 = new SubTableVo();
		po3.setTableName("oms_order_container"); //表名
		po3.setEntityName("ContainerInfo");			//实体名
		po3.setEntityPackage("oms"); 				//包名
		po3.setFtlDescription("货柜信息");			//描述
		//子表外键参数配置
		/*说明: 
		 * a) 子表引用主表主键ID作为外键，外键字段必须以_ID结尾;
		 * b) 主表和子表的外键字段名字，必须相同（除主键ID外）;
		 * c) 多个外键字段，采用逗号分隔;
		*/
		po3.setForeignKeys(new String[]{"order_id"});
		subTables.add(po3);
		mainTable.setSubTables(subTables);
		
		//[4].子表五
		SubTableVo po4 = new SubTableVo();
		po4.setTableName("oms_order_file"); //表名
		po4.setEntityName("OrderFile");			//实体名
		po4.setEntityPackage("oms"); 				//包名
		po4.setFtlDescription("附件");			//描述
		//子表外键参数配置
		/*说明: 
		 * a) 子表引用主表主键ID作为外键，外键字段必须以_ID结尾;
		 * b) 主表和子表的外键字段名字，必须相同（除主键ID外）;
		 * c) 多个外键字段，采用逗号分隔;
		*/
		po4.setForeignKeys(new String[]{"order_id"});
		subTables.add(po4);
		mainTable.setSubTables(subTables);
		
		//第三步：一对多(父子表)数据模型,代码生成
		try {
			new CodeGenerateOneToMany(mainTable,subTables).generateCodeFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
