package cn.myapps.msg;

import cn.myapps.msg.enums.MsgTypeEnum;
import cn.myapps.msg.sender.IMsgSender;
import cn.myapps.msg.sender.MsgSenderFactory;
import cn.myapps.msg.vo.MsgCommon;
import cn.myapps.msg.vo.SendResult;
import cn.myapps.msg.vo.WxTemplateData;
import com.alibaba.fastjson.JSON;
import org.apache.activemq.command.ActiveMQQueue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.Destination;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MsgWxMpTest {

	private int count=8;

	@Value("${activemq.queue.env}")
	private String queueSuffixName;

	@Autowired
	private JmsMessagingTemplate jmsMessagingTemplate;

	@Test
	public void testSend() {

	}

	@Test
	public void testSendByTemplate() {
		IMsgSender sender = MsgSenderFactory.getMsgSender(MsgTypeEnum.MP_TEMPLATE.getCode());

		SendResult result2 = sender.sendByTemplate(newMsgCommon());
		assertTrue(result2.isSuccess());
	}

	@Test
	public void testSendByMQ() {
		String ququeName = "my.msg.queue."+queueSuffixName;

		Destination destination = new ActiveMQQueue(ququeName);
		jmsMessagingTemplate.convertAndSend(destination, JSON.toJSONString(newMsgCommon()));
	}

	private MsgCommon newMsgCommon() {
		// 模板模式发送
		MsgCommon msg = new MsgCommon();
		msg.setMsgType(MsgTypeEnum.MP_TEMPLATE.getCode()); // 消息类型必须
		msg.setFrom("云枢纽"); // from必须，需要根据公众号名称查找配置
		msg.setTo("o6q3ewfCUdQh_J5TM2S3p-g7NiiI"); // openId必须(nicholas手机)
		msg.setTemplateCode("rmRiBRZ4DiuxXgUZ3J6wuLxcPo9fTVSm9nW23z0g94Q"); // 模板消息ID必须
		msg.setBizNo("订单号20200612000");
		msg.setCompanyUid("企业UID");

		List<WxTemplateData> datas = new ArrayList<>();
		datas.add(new WxTemplateData("first", "尊敬的客户，您好", null));
		datas.add(new WxTemplateData("keyword1", "20200612000"+count, null));
		datas.add(new WxTemplateData("keyword2", "货物已抵达XX，准备送往下一站。", null));
		datas.add(new WxTemplateData("remark", "2020-06-21 08:08:08", null));

		// 公众号模板消息必须这参数
//		msg.getOtherParams().put("appId", "wxd75da243a621608f"); // 改为直接根据From查找公众号配置
		msg.getOtherParams().put("templateDataList", datas);
		// 关联小程序
		//msg.getOtherParams().put("maAppid", "xxx");
		//msg.getOtherParams().put("maPagePath", "xxx");

		return msg;
	}

	private MsgCommon newMsgCommon2() {
		// 模板模式发送
		MsgCommon msg2 = new MsgCommon();
		msg2.setMsgType(MsgTypeEnum.MP_TEMPLATE.getCode()); // 消息类型必须
		msg2.setFrom("智货运");
		// oh_uY03MoNGne0ARrHjS_Jc0dMX8
		// om74Qw91TyphMV_08vZJ_xhtr-dc
		msg2.setTo("om74Qw91TyphMV_08vZJ_xhtr-dc"); // openId必须(nicholas手机)
		msg2.setTemplateCode("iMBoehZLGpncqEzRhee_QuoK0MbtWMwyxM0vSvGlVX8"); // 模板消息ID必须
		msg2.setBizNo("OCD181130144854032"+count);
		msg2.setCompanyUid("企业UID");

		List<WxTemplateData> datas = new ArrayList<>();
		datas.add(new WxTemplateData("first", "委托单号：OCD181130144854032"+count, null));
		datas.add(new WxTemplateData("keyword1", "进口", null));
		datas.add(new WxTemplateData("keyword2", "港区运输", null));
		datas.add(new WxTemplateData("keyword3", "预计装车时间", null));
		datas.add(new WxTemplateData("remark", "12:29:20装货地址：广东省广州市天河区东圃镇", null));

		// 公众号模板消息必须这参数
//		msg2.getOtherParams().put("appId", "wxb7e28d1bb1edd069");
		msg2.getOtherParams().put("templateDataList", datas);

		return msg2;
	}
}
