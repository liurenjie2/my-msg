package cn.myapps.msg;

import cn.myapps.msg.enums.MsgTypeEnum;
import cn.myapps.msg.sender.IMsgSender;
import cn.myapps.msg.sender.MsgSenderFactory;
import cn.myapps.msg.vo.MsgCommon;
import cn.myapps.msg.vo.SendResult;
import cn.myapps.msg.vo.WxTemplateData;
import com.alibaba.fastjson.JSON;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.Destination;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MsgWxCpTest {

	private int count=1;

	@Value("${activemq.queue.env}")
	private String queueSuffixName;

	@Autowired
	private JmsMessagingTemplate jmsMessagingTemplate;

	@Test
	public void testSend() {
		IMsgSender sender = MsgSenderFactory.getMsgSender(MsgTypeEnum.WX_CP.getCode());

		SendResult result = sender.send(newMsgCommon());
		assertTrue(result.isSuccess());
	}

	@Test
	public void testSendByTemplate() {
	}

	@Test
	public void testSendByMQ() {
		String ququeName = "my.msg.queue."+queueSuffixName;

		Destination destination = new ActiveMQQueue(ququeName);
		jmsMessagingTemplate.convertAndSend(destination, JSON.toJSONString(newMsgCommon()));
	}

	@Test
	public void testSendByMQTopic() {
		String topicName = "my.msg.topic."+queueSuffixName;

		Destination destination = new ActiveMQTopic(topicName);
		jmsMessagingTemplate.convertAndSend(destination, JSON.toJSONString(newMsgCommon()));
	}

	private MsgCommon newMsgCommon() {
		// 模板模式发送
		MsgCommon msg2 = new MsgCommon();
		msg2.setMsgType(MsgTypeEnum.WX_CP.getCode()); // 消息类型必须
		msg2.setFrom("云枢纽"); // from必须, 企业微信应用名称
		msg2.setTo("zhen_002"); // 企业微信账号
		//msg2.setTemplateCode("-1"); // TODO:未实现模板发送模式
		msg2.setBizNo("订单号202006210001");
		msg2.setCompanyUid("企业UID");
		msg2.setTitle("测试企业微信消息推送");
		msg2.setContent("你好，这是一条企业微信推消息"+count);

		// 企业微信agentId
//		msg2.getOtherParams().put("agentId", "1000007"); // 改为直接根据From查找企业微信配置

		return msg2;
	}
}
